#include<thread>
#include<vector>
#include<mutex>
#include<iostream>
using namespace std;


class Wallet
{
	int Money;
	std::mutex mutex;
public:
	Wallet() :Money(0) {}

	void addMoney(const int money)
	{
		std::lock_guard<std::mutex> lockGuard(mutex);

		for (int i = 0; i < money; ++i)
		{
			Money++;

		}
	};

	int getMoney()
	{
		return Money;

	}

};


int ThreadWallet()
{

	Wallet walletObject;
	std::vector<std::thread> threadsVector;

	for (int i = 0; i < 9; ++i) {
		
		threadsVector.push_back(std::thread(&Wallet::addMoney, &walletObject, 100));
	}
	for (int i = 0; i < threadsVector.size(); i++)

	{
		threadsVector.at(i).join();//
	}

	return walletObject.getMoney();

}
int main()
{

	int val = 0;
	
	for (int add = 0; add < 100; add++)
	{
		val = ThreadWallet();
		if (val != 9000)
		{
			std::cout << "Error  = " << add << std::endl;
			std::cout << "Total money = " << val << std::endl;
		}

	}
	std::cout << "Total money = " << val << std::endl;
	return 0;
}


