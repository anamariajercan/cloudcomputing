#pragma once
class Wallet
{
private:
	int money;
public:
	Wallet()=default;
	Wallet(int mo);
	int getMoney() const;
	int addMoney(const int mon);
	~Wallet()=default;

};