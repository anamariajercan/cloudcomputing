#pragma once
#include <vector>
template<typename T>
class Utils
{
public:
	struct Comparator
	{
		bool operator()(const Utils<T>&left, const Utils<T>& right) const
		{
			return left.value > right.value;
		}
	};
	T value;
	Utils() = default;
	Utils(T myValue) :value(myValue) {}
	
	T mf_iMax(T first, T second)
	{
		return ((first > second) ? first : second);
	}
	std::vector<T> myVector;
	
	T retMean(std::vector<T> v) 
	{
		for (typename std::vector<T>::iterator it = v.begin(); it != v.end(); ++it)
		{
			auto const mid = v.begin() + std::distance(v.begin(), v.end()) / 2;
		
			return *mid;
		}
	}
	void print(std::vector<T> ar)
	{
		for (auto x : ar) std::cout << x << " ";
		std::cout << std::endl;
	}
	~Utils() = default;
};