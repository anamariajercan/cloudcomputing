#include <iostream>
#include <map>
#include <fstream>
#include<ostream>
#include <string>
#include "Vehicle.h"

int main()
{
	{
		Vehicles vehicle1("Opel", 2010);
		Vehicles vehicle2("Seat", 2013);
		Vehicles vehicle3("Kia", 2012);
		Vehicles vehicle4("Dacia", 2002);
		Vehicles vehicle5("Audi", 2017);
		std::map<Vehicles, std::string, Vehicles::Comparator> vehiclesMap;
		vehiclesMap.insert(std::make_pair(vehicle1, "Opel"));
		vehiclesMap.insert(std::make_pair(vehicle2, "Seat"));
		vehiclesMap.insert(std::make_pair(vehicle3, "Kia"));
		vehiclesMap.insert(std::make_pair(vehicle4, "Dacia"));
		vehiclesMap.insert(std::make_pair(vehicle5, "Audi"));
		
		for (auto pair : vehiclesMap)
		{
			std::cout << pair.first.getYear() <<" from " << pair.second << "\n";
		}
		std::cout << '\n';
	}
	return 0;
};