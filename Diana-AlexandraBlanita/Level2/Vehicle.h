#pragma once
#include <string>
#include <iostream>
class Vehicles
{ 
private:
std::string brand;
int year;

public:
	struct Comparator
	{
		bool operator()(const Vehicles&left, const Vehicles& right) const
		{
			return left.year< right.year;
		}
	};
	Vehicles(); 
	Vehicles(std::string br, int yr);

std::string  getBrand() const;
int getYear() const;
~Vehicles(); 

};
