#include <iostream>
#include "Wall.h"
#include <thread>
#include<vector>
#include <algorithm>
#include <mutex>
Wallet wallet(10);

std::mutex mtx;

void myMethod()
{ 
	std::lock_guard<std::mutex> lock(mtx);
	for (int index = 0; index < 10; ++index)
	{
		wallet.addMoney(5);
	}
};


int main()
{
	{
		std::vector<std::thread> myVectorOfThreads;
		for (int index = 0; index < 100; ++index)
		{
			myVectorOfThreads.push_back(std::thread(myMethod));
			std::cout << std::this_thread::get_id() << std::endl;
			std::cout << wallet.getMoney() << "lei" << std::endl;
		}

		std::for_each(std::begin(myVectorOfThreads), std::end(myVectorOfThreads), std::mem_fn(&std::thread::join));
		//for (auto& th : myVectorOfThreads) th.join();

	}

};