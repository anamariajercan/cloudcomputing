#pragma once

namespace Framework
{
	class IDataPacket
	{
	public:
		virtual ~IDataPacket() = default;
		IDataPacket() = default;

		virtual std::string getContentAsString() const = 0;
	};
}