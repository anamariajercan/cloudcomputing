#pragma once
#include "Response.h"

class BirthstonesResponse : public Framework::Response
{
public:
	BirthstonesResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};