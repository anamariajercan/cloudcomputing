#pragma once
#include "Response.h"

class ZodiacResponse : public Framework::Response
{
public:
	ZodiacResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};