#pragma once
#include "Response.h"

class ChineseZodiacResponse : public Framework::Response
{
public:
	ChineseZodiacResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};
