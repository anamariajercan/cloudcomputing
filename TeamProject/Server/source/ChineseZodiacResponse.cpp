#include "ChineseZodiacResponse.h"

ChineseZodiacResponse::ChineseZodiacResponse(): Response("ChineseZodiac")
{

}
std::string ChineseZodiacResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
auto year = atoi(packet.get<std::string>("Year").c_str());
std::string chinese_sign = "";
switch ((year - 1936) % 12)
{
default:(year > 1936 || year < 2055);
	chinese_sign = "you entered an invalid year, please try again!"; break;
case 0:
	chinese_sign = "Your chinese zodiac sign is the rat! Rats are very popular!"; break;
case 1:
	chinese_sign = "Your chinese zodiac sign is the ox! Oxes are dependable and calm!"; break;
case 2:
	chinese_sign = "Your chinese zodiac sign is the tiger!Tigers are brave and respected!"; break;
case 3:
	chinese_sign = "Your chinese zodiac sign is the rabbit! Rabits are nice to be around!"; break;
case 4:
	chinese_sign = "Your chinese zodiac sign is the dragon! Dragons are known for their good health and having lots of energy!"; break;
case 5:
	chinese_sign = "Your chinese zodiac sign is the snake!Snakes are good with money!"; break;
case 6:
	chinese_sign = "Your chinese zodiac sign is the horse!Horses are popular, cheerful, and quick to compliment others!"; break;
case 7:
	chinese_sign = "Your chinese zodiac sign is the goat! Goats are known for being great artists!"; break;
case 8:
	chinese_sign = "Your chinese zodiac sign is the monkey!Monkeys are very funny and good problem solvers"; break;
case 9:
	chinese_sign = "Your chinese zodiac sign is the rooster!Roosters are talented and hard working!"; break;
case 10:
	chinese_sign = "Your chinese zodiac sign is the dog! Dogs are very loyal and can keep a secret!"; break;
case 11:
	chinese_sign = "Your chinese zodiac sign is the pig! Pigs are good students, honest and brave!"; break;
}
this->content.push_back(boost::property_tree::ptree::value_type("File", "chinesesign.txt"));
this->content.push_back(boost::property_tree::ptree::value_type("Result", chinese_sign));

return this->getContentAsString();
}

