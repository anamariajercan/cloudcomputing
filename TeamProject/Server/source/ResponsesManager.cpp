#include "ResponsesManager.h"
#include "HandshakeResponse.h"
#include "SumResponse.h"
#include "WordCountResponse.h"
#include "ZodiacResponse.h"
#include "ChineseZodiacResponse.h"
#include "BirthstonesResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
	this->Responses.emplace("Sum", std::make_shared<SumResponse>());
	this->Responses.emplace("WordCounter", std::make_shared<WordCountResponse>());
	this->Responses.emplace("ZodiacIdentifier", std::make_shared<ZodiacResponse>());
	this->Responses.emplace("ChineseZodiac", std::make_shared<ChineseZodiacResponse>());
	this->Responses.emplace("Birthstones", std::make_shared<BirthstonesResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
