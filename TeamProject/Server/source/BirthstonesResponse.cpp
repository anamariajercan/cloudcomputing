#include "BirthstonesResponse.h"
#include <boost/date_time/date_iterator.hpp>


BirthstonesResponse::BirthstonesResponse() : Response("Birthstones")
{
}

std::string BirthstonesResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto month = atoi(packet.get<std::string>("Month").c_str());
	auto day = atoi(packet.get<std::string>("Day").c_str());
	std::string birthstones_sign = "";
	switch (month) {
	case 12:  (day >= 22) ? birthstones_sign = "Ruby" : birthstones_sign = "Citrine"; break;
	case 11:  (day >= 23) ? birthstones_sign = "Citrine" : birthstones_sign = "Beryl"; break;
	case 10:  (day >= 23) ? birthstones_sign = "Beryl" : birthstones_sign = "Chrysolite"; break;
	case 9:   (day >= 23) ? birthstones_sign = "Chrysolite" : birthstones_sign = "Carnelian"; break;
	case 8:   (day >= 23) ? birthstones_sign = "Carnelian" : birthstones_sign = "Onyx"; break;
	case 7:   (day >= 23) ? birthstones_sign = "Onyx" : birthstones_sign = "Emerald"; break;
	case 6:   (day >= 22) ? birthstones_sign = "Emerald" : birthstones_sign = "Agate"; break;
	case 5:   (day >= 22) ? birthstones_sign = "Agate" : birthstones_sign = "Sapphire"; break;
	case 4:   (day >= 21) ? birthstones_sign = "Sapphire" : birthstones_sign = "Bloodstone"; break;
	case 3:   (day >= 21) ? birthstones_sign = "Bloodstone" : birthstones_sign = "Amethyst"; break;
	case 2:   (day >= 20) ? birthstones_sign = "Amethyst" : birthstones_sign = "Garnet"; break;
	case 1:   (day >= 21) ? birthstones_sign = "Garnet" : birthstones_sign = "Ruby"; break;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "BirthstonesResponse.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", birthstones_sign));

	return this->getContentAsString();
}
