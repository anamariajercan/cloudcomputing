#include "ZodiacResponse.h"
#include <boost/date_time/date_iterator.hpp>

ZodiacResponse::ZodiacResponse() : Response("ZodiacIdentifier")
{

}

std::string ZodiacResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto month = atoi(packet.get<std::string>("Month").c_str());
	auto day = atoi(packet.get<std::string>("Day").c_str());
	std::string astro_sign ="";
	switch (month) {
	case 12:  (day >= 22) ? astro_sign = "Capricorn" : astro_sign = "Sagittarius"; break;
	case 11:  (day >= 23) ? astro_sign = "Sagittarius" : astro_sign = "Scorpio"; break;
	case 10:  (day >= 23) ? astro_sign = "Scorpio" : astro_sign = "Libra"; break;
	case 9:   (day >= 23) ? astro_sign = "Libra" : astro_sign = "Virgo"; break;
	case 8:   (day >= 23) ? astro_sign = "Virgo" : astro_sign = "Leo"; break;
	case 7:   (day >= 23) ? astro_sign = "Leo" : astro_sign = "Cancer"; break;
	case 6:   (day >= 22) ? astro_sign = "Cancer" : astro_sign = "Gemini"; break;
	case 5:   (day >= 22) ? astro_sign = "Gemini" : astro_sign = "Taurus"; break;
	case 4:   (day >= 21) ? astro_sign = "Taurus" : astro_sign = "Aries"; break;
	case 3:   (day >= 21) ? astro_sign = "Aries" : astro_sign = "Pisces"; break;
	case 2:   (day >= 20) ? astro_sign = "Pisces" : astro_sign = "Aquarius"; break;
	case 1:   (day >= 21) ? astro_sign = "Aquarius" : astro_sign = "Capricorn"; break;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "astrosign.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", astro_sign));

	return this->getContentAsString();
}
