#include "RequestsManager.h"

#include "HandshakeRequest.h"
#include "SumRequest.h"
#include "WordCountRequest.h"
#include "ZodiacRequest.h"
#include "ChineseZodiacRequest.h"
#include "BirthstonesRequest.h"
#include <map>

RequestsManager::RequestsManager()
{
	this->requests.emplace("Handshake", std::make_shared<HandshakeRequest>());
	this->requests.emplace("Sum", std::make_shared<SumRequest>());
	this->requests.emplace("WordCounter", std::make_shared<WordCountRequest>());
	this->requests.emplace("ZodiacIdentifier", std::make_shared<ZodiacRequest>());
	this->requests.emplace("ChineseZodiac", std::make_shared<ChineseZodiacRequest>());
	this->requests.emplace("Birthstones", std::make_shared<BirthstonesRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
