#pragma once
#include "ChineseZodiacRequest.h"
#include <iostream>
using namespace std;

ChineseZodiacRequest::ChineseZodiacRequest():Request("ChineseZodiac")
{
	string year;
	cout << "Please enter a year for which you want to find chinese zodiac: ";
	cin >> year;
	this->content.push_back(boost::property_tree::ptree::value_type("Year", year));
}
