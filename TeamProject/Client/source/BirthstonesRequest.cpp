
#include "BirthstonesRequest.h"

BirthstonesRequest::BirthstonesRequest() : Request("Birthstones")
{
	this->content.push_back(boost::property_tree::ptree::value_type("Day", "7"));
	this->content.push_back(boost::property_tree::ptree::value_type("Month", "7"));

}
