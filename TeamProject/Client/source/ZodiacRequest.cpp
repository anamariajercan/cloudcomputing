#include "ZodiacRequest.h"
#include <iostream>
using namespace std;
ZodiacRequest::ZodiacRequest() : Request("ZodiacIdentifier")
{
	string day;
	string month;
	std::cout << "Add a day for which you want to find the zodiac:";
	std::cin >> day;
	std::cout << "Add a month for which you want to find the zodiac:";
	std::cin >> month;
	this->content.push_back(boost::property_tree::ptree::value_type("Day", day));
	this->content.push_back(boost::property_tree::ptree::value_type("Month", month));

}
