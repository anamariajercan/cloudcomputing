#include <thread>
#include <vector>
#include "wallet.h"
#include <iostream>
#include <mutex>
std::mutex mtx;
wallet wallet_(5);

void myFunction()
{
	std::lock_guard<std::mutex> lck(mtx);
	for (int index = 0; index < 1000; ++index)
	{
		wallet_.add_money(index);
	}
}
int main()
{
	std::vector <std::thread> myVectorOfThread;
	for (int i = 0; i < 100; ++i)
	{
		myVectorOfThread.emplace_back(myFunction);
		std::cout << myVectorOfThread[i].get_id() << std::endl;
		std::cout << wallet_.get_money() << "lei" << std::endl;

	}
	for (auto& th : myVectorOfThread) th.join();

	return 0;
}
