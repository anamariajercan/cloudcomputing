#pragma once
class wallet
{
private:
	int monney;
public:
	wallet() = default;
	wallet(int moneys);
	void add_money(const int moneys);
	int get_money() const;
};