#pragma once
#include "Utils.h"
#include <vector>
#include <iostream>
#include <algorithm>

template<typename T>
class UtilsExt : public Utils<T>
{
private:
	std::vector<T> mf_myListOfElements;
public:
	UtilsExt() = default;
	~UtilsExt() = default;
	void set_mf_myListOfElements(T myElement)
	{
		mf_myListOfElements.push_back(myElement);
	};
	T get_mf_myMean()
	{
			auto median_it2 = mf_myListOfElements.begin() + mf_myListOfElements.size() / 2;
			return *median_it2;
	};
};
