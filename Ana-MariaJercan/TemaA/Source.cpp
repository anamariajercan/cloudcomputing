#include  "UtilsExt.h"
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

int main()
{
	ifstream fin("example.txt");
	if (fin.is_open())
	{
		UtilsExt<int> vectorElements;
		string str;
		while (!fin.eof())
		{
			while (getline(fin, str))
			{
				int val;
				stringstream in(str);
				while (in >> val) {
					vectorElements.set_mf_myListOfElements(val);
				}
				cout << str << '\n';
			}
		}
		cout<<vectorElements.get_mf_myMean();
		fin.close();
	}
	return 0;
}

//{
	//	auto utils = Utils<double>();
	//	std::cout << utils.mf_iMax(3.7, 6.3);
	//	std::cout << '\n';
	//}
	//{
	//	map<int, std::string> my_map;
	//	my_map[6] = "six";
	//	my_map.insert(std::make_pair(1, "one"));
	//	my_map.emplace(std::make_pair(2, "two"));
	//	for (auto element : my_map)
	//	{
	//		std::cout << element.first << " " << element.second << std::endl;
	//	}
	//	std::cout << my_map[6];
	//	std::cout << '\n';
	//}
	//{
	//	std::map <Utils<int>, int, Utils<int>::Comparator> my_map;
	//	my_map[Utils<int>(7)] = 4;
	//	my_map[Utils<int>(1)] = 3;
	//	for (auto element : my_map)
	//	{
	//		std::cout << element.first.value << " " << element.second << std::endl;
	//	}
	//}

	//{
	//	/*smart pointer*/
	//	auto x = std::make_unique<int>(5);
	//	std::cout << *x;
	//	//nu poti sa il folosesti decat in scop dar trebuie sa il distrugi
	//}
	//{
	//	std::shared_ptr<int > y;
	//	{
	//	auto x = std::make_shared<int>(5);
	//	std::cout << *x;
	//	auto y = x;
	//	}
	//	std::cout << *y;

	//}
