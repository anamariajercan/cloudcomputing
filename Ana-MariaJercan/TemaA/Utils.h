#pragma once
template<typename T>
class Utils
{
public:
	struct Comparator
	{
		bool operator()(const Utils<T>&left, const Utils<T>& right) const
		{
			return left.value > right.value;
		}
	};
	T value;
	Utils() = default;
	Utils(T myValue) :value(myValue) {}
	T mf_iMax(T first, T second)
	{
		return ((first > second) ? first : second);
	}
	~Utils() = default;
};