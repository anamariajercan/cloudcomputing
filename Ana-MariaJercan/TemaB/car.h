#pragma once
#include <string>;
using namespace std;
class car
{
private:
	string mf_brand;
	
public:
	int mf_year;
	double mf_price;
	bool mf_full_option;
	car()=default;
	~car() = default;
	car(string brand, int year, double price, bool full_option);
	string get_mf_brand() const;
	struct comparator
	{
		bool operator()(const car& left, const car& right) const;
	};
};


