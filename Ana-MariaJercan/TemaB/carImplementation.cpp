#include "car.h"

car::car(const string brand, const int year, const double price, const bool full_option)
{
	mf_brand = brand;
	mf_year = year;
	mf_price = price;
	mf_full_option = full_option;
}

string car::get_mf_brand() const
{
	return mf_brand;
}

bool car::comparator::operator()(const car& left, const car& right) const
{
	//return left.mf_year < right.mf_year;
	if (left.mf_price - right.mf_price > 1000)
	{
		return left.mf_price < right.mf_price;
	}
	else if(left.mf_full_option != right.mf_full_option)
	{
		return left.mf_full_option < right.mf_full_option;
	}
	else if (left.mf_full_option && right.mf_full_option)
	{
		return left.mf_year < right.mf_year;
	}
	else 
	{
		return left.mf_brand.compare(right.mf_brand);
	}
}
