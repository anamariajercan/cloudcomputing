#include "car.h"
#include <map>
#include <iostream>

int main()
{
	car car1 = car("BMW", 2011, 12000,true);
	car car2 = car("Toyota", 2015, 12500, false);
	car car3 = car("mercedes", 2013, 15000, true);
	car car4 = car("Dacia", 2011, 5000, false);
	car car5 = car("Toyota", 2017, 16000, true);
	map<car, string, car::comparator> my_map;
	my_map.insert(std::make_pair(car1, car1.get_mf_brand()));
	my_map.insert(std::make_pair(car2, car2.get_mf_brand()));
	my_map.insert(std::make_pair(car3, car3.get_mf_brand()));
	my_map.insert(std::make_pair(car4, car4.get_mf_brand()));
	my_map.insert(std::make_pair(car5, car5.get_mf_brand()));

	for (auto const& pair : my_map)
	{
		cout << pair.first.get_mf_brand() << pair.first.mf_price << " represented by object at " << pair.second << "\n";
	}
	return 0;
}
